include .env

#
# Fixed versions to use
#
php_version_74 = 7.4.33
php_version_80 = 8.0.30
php_version_81 = 8.1.31
php_version_82 = 8.2.27
php_version_83 = 8.3.15
php_version_84 = 8.4.2
composer_version_74 = 2.8.4
composer_version_80 = 2.8.4
composer_version_81 = 2.8.4
composer_version_82 = 2.8.4
composer_version_83 = 2.8.4
composer_version_84 = 2.8.4
xdebug_version_74 = 3.1.6
xdebug_version_80 = 3.4.0
xdebug_version_81 = 3.4.0
xdebug_version_82 = 3.4.0
xdebug_version_83 = 3.4.0
xdebug_version_84 = 3.4.0

#
# Default target, will run when executing make without arguments
#
.PHONY: check
check: cs stan test-compatibility

#
# General
#
.PHONY: init
init: docker-init vendor

.PHONY: dist-clean
dist-clean: docker-clean vendor-clean

.PHONY: dist-fresh
dist-fresh: dist-clean init

#
# Tools
#
.PHONY: cs
cs: vendor
	@./scripts/phpcs.sh

.PHONY: stan
stan: vendor
	@./scripts/phpstan.sh analyse

.PHONY: test
test: vendor
	@./scripts/phpunit.sh

.PHONY: test-compatibility
test-compatibility: vendor build-additional-test-images
	PHP_VERSION=7.4 ./scripts/phpunit.sh
	PHP_VERSION=8.0 ./scripts/phpunit.sh
	PHP_VERSION=8.1 ./scripts/phpunit.sh
	PHP_VERSION=8.2 ./scripts/phpunit.sh
	PHP_VERSION=8.3 ./scripts/phpunit.sh
	PHP_VERSION=8.4 ./scripts/phpunit.sh

#
# dotenv
#
.env:
	@cp .env.example .env
	@echo "HOST_USER_UID=$(shell id -u)" >> .env
	@echo "HOST_USER_GID=$(shell id -g)" >> .env
	@read -p "Enter project name: " project_name; \
		sed -i -e "s/COMPOSE_PROJECT_NAME=/COMPOSE_PROJECT_NAME=$${project_name}/g" .env

#
# Composer
#
vendor: composer.json
	@$(MAKE) composer-install

.PHONY: composer-install
composer-install:
	@./scripts/tools.sh composer install

.PHONY: vendor-clean
vendor-clean:
	@-rm -rf ./vendor/
	@-rm -f ./composer.lock

.PHONY: remove-cache-files
remove-cache-files:
	@-rm -f ./*.cache

#
# Docker
#
.PHONY: up
up:
	@docker compose --profile dev-environment up -d

.PHONY: down
down:
	@docker compose --profile dev-environment --profile tools down

.PHONY: docker-init
docker-init: build-images xdebug-ini

.PHONY: docker-clean
docker-clean: down remove-xdebug-ini remove-cache-files remove-containers remove-images

.PHONY: build-images
build-images: build-tools-image

.PHONY: build-tools-image
build-tools-image:
	@docker build \
		--progress=plain \
        --pull \
		--build-arg PHP_VERSION=${php_version_74} \
		--build-arg COMPOSER_VERSION=${composer_version_74} \
		--build-arg XDEBUG_VERSION=${xdebug_version_74} \
		-t "${COMPOSE_PROJECT_NAME}-tools-7.4" \
		./docker/php/tools/

.PHONY: build-additional-test-images
build-additional-test-images:
	@docker build \
		--progress=plain \
        --pull \
		--build-arg PHP_VERSION=${php_version_80} \
		--build-arg COMPOSER_VERSION=${composer_version_80} \
		--build-arg XDEBUG_VERSION=${xdebug_version_80} \
		-t "${COMPOSE_PROJECT_NAME}-tools-8.0" \
		./docker/php/tools/
	@docker build \
		--progress=plain \
        --pull \
		--build-arg PHP_VERSION=${php_version_81} \
		--build-arg COMPOSER_VERSION=${composer_version_81} \
		--build-arg XDEBUG_VERSION=${xdebug_version_81} \
		-t "${COMPOSE_PROJECT_NAME}-tools-8.1" \
		./docker/php/tools/
	@docker build \
		--progress=plain \
        --pull \
		--build-arg PHP_VERSION=${php_version_82} \
		--build-arg COMPOSER_VERSION=${composer_version_82} \
		--build-arg XDEBUG_VERSION=${xdebug_version_82} \
		-t "${COMPOSE_PROJECT_NAME}-tools-8.2" \
		./docker/php/tools/
	@docker build \
		--progress=plain \
        --pull \
		--build-arg PHP_VERSION=${php_version_83} \
		--build-arg COMPOSER_VERSION=${composer_version_83} \
		--build-arg XDEBUG_VERSION=${xdebug_version_83} \
		-t "${COMPOSE_PROJECT_NAME}-tools-8.3" \
		./docker/php/tools/
	@docker build \
		--progress=plain \
        --pull \
		--build-arg PHP_VERSION=${php_version_84} \
		--build-arg COMPOSER_VERSION=${composer_version_84} \
		--build-arg XDEBUG_VERSION=${xdebug_version_84} \
		-t "${COMPOSE_PROJECT_NAME}-tools-8.4" \
		./docker/php/tools/

.PHONY: xdebug-ini
xdebug-ini:
	@./scripts/tools.sh ./scripts/generate-xdebug-ini.sh > ./xdebug.ini

.PHONY: remove-containers
remove-containers:
	@./scripts/remove-containers.sh "${COMPOSE_PROJECT_NAME}_*"

.PHONY: remove-images
remove-images:
	@./scripts/remove-images.sh "${COMPOSE_PROJECT_NAME}-*"

.PHONY: remove-xdebug-ini
remove-xdebug-ini:
	@-rm -f ./xdebug.ini
