<?php

namespace SomeNamespace;

/**
 * @return int
 */
function GenericAcceptanceFunction1() {

    return GenericAcceptanceFunction2(42);

}

/**
 * @param int $number
 * @return int
 */
function GenericAcceptanceFunction2($number) {

    return ($number % 5) * 42;

}
