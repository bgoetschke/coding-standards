<?php

namespace SomeNamespace;

class WhitespaceTypecastClass {

    /**
     * @var string
     */
    private $value = '';

    /**
     * @param string $value
     */
    public function __construct($value) {

        $this->value = (string)$value;

    }

    /**
     * @param string $value
     */
    public function updateValue($value) {

        $this->value = (string) $value;

    }

}
