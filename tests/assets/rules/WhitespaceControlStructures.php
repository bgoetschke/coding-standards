<?php

namespace SomeNamespace;

/**
 * @param mixed[] $data
 * @return mixed
 */
function WhitespaceControlStructuresFunction1(array $data) {

    foreach($data as $index => $value) {
        if($value === null) {
            $data[$index] = 42;
        }
    }
    return $data;

}

/**
 * @param mixed[] $data
 * @return bool
 */
function WhitespaceControlStructuresFunction2(array $data) {

    foreach ($data as $value) {
        switch($value) {
            case null:
            case 42:
                return false;
        }
    }
    return true;

}
