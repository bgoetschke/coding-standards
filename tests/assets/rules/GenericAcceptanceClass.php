<?php

namespace SomeNamespace;

class GenericAcceptanceClass {

    public function __construct() {
    }

    /**
     * @param string[] $data
     * @return bool
     */
    public function doSomething(array $data) {

        foreach ($data as $entry) {
            if (!$this->privateAction($entry)) {
                return false;
            }
        }

        return true;

    }

    /**
     * @param string $entry
     * @return bool
     */
    private function privateAction($entry) {

        switch ($entry) {
            case null:
            case 42:
                return true;
        }

        if (is_array($entry)) {
            foreach ($entry as $index => $value) {
                if (!is_numeric($index) || !is_scalar($value)) {
                    return false;
                }
            }
            return true;
        }

        for ($i = 43; $i < 55; $i++) {
            if ($entry === $i) {
                return false;
            }
        }

        if ($entry === 'hello world') {
            return false;
        }

        if (is_string($entry)) {
            $x = 0;
            while ($x < strlen($entry)) {
                if ($entry[$x] === 'x') {
                    return false;
                }
            }
        }

        return true;

    }

}
