<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\CodingStandards\Rules;

use BjoernGoetschke\Test\CodingStandards\TestFile;
use PHP_CodeSniffer\Standards\Generic\Sniffs\Formatting\NoSpaceAfterCastSniff;
use PHP_CodeSniffer\Standards\Squiz\Sniffs\ControlStructures\ControlSignatureSniff;
use PHPUnit\Framework\TestCase;

final class WhitespacesTest extends TestCase
{
    public function testWhitespacesOnTypecasts(): void
    {
        $file = TestFile::forLocalFile(
            dirname(__DIR__, 2) . '/assets/rules/WhitespaceTypecast.php',
        );

        $file->assertError(
            26,
            24,
            NoSpaceAfterCastSniff::class,
            'Generic.Formatting.NoSpaceAfterCast.SpaceFound',
        );

        $file->assertAllWarningsAndErrorsAsserted();
    }

    public function testWhitespacesOnControlStructures(): void
    {
        $file = TestFile::forLocalFile(
            dirname(__DIR__, 2) . '/assets/rules/WhitespaceControlStructures.php',
        );

        $file->assertError(
            11,
            5,
            ControlSignatureSniff::class,
            'Squiz.ControlStructures.ControlSignature.SpaceAfterKeyword',
        );

        $file->assertError(
            12,
            9,
            ControlSignatureSniff::class,
            'Squiz.ControlStructures.ControlSignature.SpaceAfterKeyword',
        );

        $file->assertError(
            27,
            9,
            ControlSignatureSniff::class,
            'Squiz.ControlStructures.ControlSignature.SpaceAfterKeyword',
        );

        $file->assertAllWarningsAndErrorsAsserted();
    }
}
