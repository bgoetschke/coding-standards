<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\CodingStandards\Rules;

use BjoernGoetschke\Test\CodingStandards\TestFile;
use PHPUnit\Framework\TestCase;

final class GenericAcceptanceTest extends TestCase
{
    public function testGenericAcceptanceClass(): void
    {
        $file = TestFile::forLocalFile(
            dirname(__DIR__, 2) . '/assets/rules/GenericAcceptanceClass.php',
        );

        $file->assertNoWarningsAndErrors();
    }

    public function testGenericAcceptanceFunctions(): void
    {
        $file = TestFile::forLocalFile(
            dirname(__DIR__, 2) . '/assets/rules/GenericAcceptanceFunctions.php',
        );

        $file->assertNoWarningsAndErrors();
    }
}
