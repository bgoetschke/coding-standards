<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\CodingStandards;

use PHP_CodeSniffer\Config;
use PHP_CodeSniffer\Exceptions\DeepExitException;
use PHP_CodeSniffer\Files\LocalFile;
use PHP_CodeSniffer\Runner;
use PHPUnit\Framework\TestCase;

final class TestFile extends LocalFile
{
    /**
     * @var array<int, array<int, array<int, mixed>>>
     */
    private array $assertedWarnings = [];

    /**
     * @var array<int, array<int, array<int, mixed>>>
     */
    private array $assertedErrors = [];

    /**
     * @param string $file
     * @return self
     * @throws DeepExitException
     */
    public static function forLocalFile(string $file): self
    {
        $runner = new Runner();
        $runner->config = new Config(
            [
                '-s',
                '--standard=' . dirname(__DIR__, 2) . '/standards/BjoernGoetschke',
            ],
        );
        $runner->init();
        $runner->ruleset->populateTokenListeners();

        $file = new self($file, $runner->ruleset, $runner->config);
        $file->process();

        return $file;
    }

    /**
     * @param int $line
     * @param int $column
     * @param string $listener
     * @param string $source
     */
    public function assertWarning(int $line, int $column, string $listener, string $source): void
    {
        TestCase::assertTrue(
            isset($this->warnings[$line][$column]),
            sprintf('Expected warning in analyzed file on line %1$d column %2$d', $line, $column),
        );

        $index = null;
        foreach ($this->warnings[$line][$column] as $currentIndex => $currentData) {
            TestCase::assertIsInt($currentIndex);
            if ($currentData['listener'] === $listener && $currentData['source'] === $source) {
                $index = $currentIndex;
                break;
            }
        }

        TestCase::assertNotNull(
            $index,
            sprintf('Specified warning in analyzed file on line %1$d column %2$d not found', $line, $column),
        );

        $this->assertedWarnings[$line][$column][$index] = $this->errors[$line][$column][$index];
    }

    /**
     * @param int $line
     * @param int $column
     * @param string $listener
     * @param string $source
     */
    public function assertError(int $line, int $column, string $listener, string $source): void
    {
        TestCase::assertTrue(
            isset($this->errors[$line][$column]),
            sprintf('Expected error in analyzed file on line %1$d column %2$d', $line, $column),
        );

        $index = null;
        foreach ($this->errors[$line][$column] as $currentIndex => $currentData) {
            TestCase::assertIsInt($currentIndex);
            if ($currentData['listener'] === $listener && $currentData['source'] === $source) {
                $index = $currentIndex;
                break;
            }
        }

        TestCase::assertNotNull(
            $index,
            sprintf('Specified error in analyzed file on line %1$d column %2$d not found', $line, $column),
        );

        $this->assertedErrors[$line][$column][$index] = $this->errors[$line][$column][$index];
    }

    public function assertAllWarningsAndErrorsAsserted(): void
    {
        TestCase::assertNotSame(
            0,
            $this->errorCount + $this->warningCount,
            'Expected at least one warning or error in analyzed file',
        );

        $unassertedWarnings = [];
        foreach ($this->warnings as $line => $colums) {
            foreach ($colums as $colum => $warnings) {
                foreach ($warnings as $warning => $data) {
                    if (!isset($this->assertedWarnings[$line][$colum][$warning])) {
                        $unassertedWarnings[$line][$colum][$warning] = $data;
                    }
                }
            }
        }

        $unassertedErrors = [];
        foreach ($this->errors as $line => $colums) {
            foreach ($colums as $colum => $errors) {
                foreach ($errors as $error => $data) {
                    if (!isset($this->assertedErrors[$line][$colum][$error])) {
                        $unassertedErrors[$line][$colum][$error] = $data;
                    }
                }
            }
        }

        TestCase::assertSame(
            [],
            $unassertedWarnings,
            'Expected no unasserted warnings in analyzed file',
        );

        TestCase::assertSame(
            [],
            $unassertedErrors,
            'Expected no unasserted errors in analyzed file',
        );
    }

    public function assertNoWarningsAndErrors(): void
    {
        TestCase::assertSame(
            0,
            $this->errorCount + $this->warningCount,
            'Expected no warnings and errors in analyzed file',
        );
    }
}
