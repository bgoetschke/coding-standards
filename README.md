# Personal coding standards (work in progress)

## Installation

Since this library has no stable release yet, it is suggested to
simply copy the file `standards/BjoernGoetschke/ruleset.xml` to your
project and reference it inside your `phpcs.xml` like this:
`<rule ref="copy.of.ruleset.xml" />`

The ruleset file contains a version identifier to easily see
if you are using the latest version.

## LICENSE

The library is released under the BSD-2-Clause license.
You can find a copy of this license in LICENSE.txt.
